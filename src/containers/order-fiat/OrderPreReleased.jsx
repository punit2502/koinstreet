import React from 'react';
import config from '../../config';


const OrderPreReleased = (props) => (
	<div className="col-xs-12 text-center order-status-section">
		<h2 style={{margin: "0"}}>We are processing your order</h2>
		<h5>Please allow up to 15 minutes.</h5>

	</div>
);

export default OrderPreReleased;
