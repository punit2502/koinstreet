const config = {
	NAME: 'Koinstreet',
	DOMAIN: 'https://koinstreet.io',
	API_BASE_URL: 'https://api.nexchange.io/en/api/v1',
	SUPPORT_EMAIL: 'support@koinstreet.io',
	PRICE_FETCH_INTERVAL: 60000,
	ORDER_DETAILS_FETCH_INTERVAL: 20000,
	RECENT_ORDERS_INTERVAL: 20000,
	RECENT_ORDERS_COUNT: 11,
	PRICE_COMPARISON_INTERVAL: 60000,
	REFERRAL_CODE: 'R1SHJFSESJ0'
};

export default config;
