import React from 'react';
import TeamMember from './TeamMember';

const About = () => (
  <div id="features">
    <div className="container">
      <div className="row">
        <div className="col-xs-12">
          <h2>Features</h2>

          <div id="team-members" className="row">
            <TeamMember id="fast" name="Fastest Instant Exchange"
              description="We are the fastest Instant exchange available. Making an exchange is as easy as counting 1,2 & 3 in your hands" />

            <TeamMember id="anonymous" name="Anonymous"
              description="No registartions or account required to exchange between cryptocurrencies." />

            <TeamMember id="cheap" name="Less Rates"
              description="With flat 0.5% transaction fees, Koinstreet offers the lowest rates available!" />

            <TeamMember id="globe" name="Trade Anytime, Anywhere"
              description="Exchange cryptocurrencies 24x7x365 across the globe instantly." />

            <TeamMember id="secure" name="Secure"
              description="We never host or hold user funds hence making us the most secure exhange." />

            <TeamMember id="stable" name="Stable"
              description="Our exchange engine has been through rigourous testing and is highly stable." />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default About;
